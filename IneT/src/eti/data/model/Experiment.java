package eti.data.model;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Experiment.
 * 
 * @author Mario Gomez-Martinez
 */
public class Experiment {
	public static enum VarRol {
		INPUT, OUTPUT, CONTROL
	}

	private final DataSet dataSet = new DataSet();
	private final List<Variable> inputs = new ArrayList<Variable>();
	private final List<Variable> outputs = new ArrayList<Variable>();
	private final List<Variable> unused = new ArrayList<Variable>();

	public List<Variable> getInputs() {
		return inputs;
	}

	// public void setInputs(List<Variable> inputs) {
	// this.inputs = inputs;
	// }

	public List<Variable> getOutputs() {
		return outputs;
	}

	// public void setOutputs(List<Variable> outputs) {
	// this.outputs = outputs;
	// }

	public List<Variable> getUnused() {
		return unused;
	}

	// public void setUnused(List<Variable> unused) {
	// this.unused = unused;
	// }

	public DataSet getDataSet() {
		return dataSet;
	}

	// public void setDataSet(DataSet dataSet) {
	// this.dataSet = dataSet;
	// }

	public void clearRoles() {
		unused.clear();
		inputs.clear();
		outputs.clear();
		for (Variable var : dataSet.getVariables().values()) {
			unused.add(var);
		}
	}
}
