/**
 * 
 */
package eti.data.model;


import java.util.HashSet;
import java.util.Set;

import eti.data.tools.SimpleArrayStatistics;

/**
 * 
 * @author Mario Gomez-Martinez
 */
public class Variable {
	private String name;
	private Statistics stats;
	private MetaData metaData;
	private double[] data;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Statistics getStatistics() {
		return stats;
	}

	public double[] getData() {
		return data;
	}

	public void setData(double[] data) {
		this.data = data;
		stats = new Statistics();
		stats.setName(name);
		computeStatistics();
	}

	private void computeStatistics() {
		SimpleArrayStatistics stat = new SimpleArrayStatistics(data);
		stats.setMean(stat.mean());
		stats.setMinValue(stat.getMin());
		stats.setMaxValue(stat.getMax());
		stats.setMedian(stat.getMedian());
		stats.setStandardDeviation(stat.getStandardDeviationForSample());
		stats.setMissingValues(stat.getMissing());
	}

	public void replaceMissing() {
		for (int i = 0; i < data.length; i++) {
			if (Double.isNaN(data[i]))
				data[i] = stats.getMean();
		}
	}

	public Set<Integer> getMissingIndices() {
		Set<Integer> missing = new HashSet<Integer>();
		for (int i = 0; i < data.length; i++) {
			if (Double.isNaN(data[i]))
				missing.add(i);
		}
		return missing;
	}

	public MetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}

	@Override
	public String toString() {
		return name;
	}

}
