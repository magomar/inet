package eti.data.model;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;

import eti.data.tools.DataTools;

/**
 * 
 * @author Mario Gomez-Martinez
 */
public class DataSet extends Observable {
	private final Map<String, Variable> variables = new LinkedHashMap<String, Variable>();

	public Map<String, Variable> getVariables() {
		return variables;
	}

	public void setData(Map<String, List<Double>> data) {
		for (Entry<String, List<Double>> entry : data.entrySet()) {
			String name = entry.getKey();
			double[] dataArray = DataTools.toDoubleArray(entry.getValue());
			if (variables.containsKey(name))
				variables.get(name).setData(dataArray);
			else {
				Variable var = new Variable();
				var.setName(name);
				var.setData(dataArray);
				MetaData metaData = new MetaData();
				metaData.setName(name);
				metaData.setSet(MetaData.DEFAULT_SET);
				var.setMetaData(metaData);
				variables.put(name, var);
			}
		}
		setChanged();
		notifyObservers();
	}

	public void setMetadata(List<MetaData> metaData) {
		for (MetaData varMetaData : metaData) {
			String name = varMetaData.getName();
			if (variables.containsKey(name))
				variables.get(name).setMetaData(varMetaData);
			else {
				Variable var = new Variable();
				var.setName(name);
				var.setMetaData(varMetaData);
				variables.put(name, var);
			}
		}
		setChanged();
		notifyObservers();
	}

	public void replaceMissing() {
		for (Entry<String, Variable> entry : variables.entrySet()) {
			entry.getValue().replaceMissing();
		}
	}

}
