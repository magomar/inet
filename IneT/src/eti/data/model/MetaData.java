package eti.data.model;

/**
 * The Class VarMetaData.
 * 
 * @author Mario Gomez-Martinez
 */
public class MetaData {

	/** The Constant FIELDS. */
	public final static String[] FIELDS = { "name", "label", "measure", "set" };
	public final static String DEFAULT_SET = "none";

	public static enum Measure {
		NOMINAL, ORDINAL, SCALE
	}

	private String name;
	private String label;
	private String measure;
	private String set;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getSet() {
		return set;
	}

	public void setSet(String set) {
		this.set = set;
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	/**
	 * Gets an array with the values of the main attributes (those included in
	 * the FIELDS).
	 * 
	 * @return the array
	 */
	public Object[] getArray() {
		Object[] array = { name, label, measure, set };
		return array;
	}

	@Override
	public String toString() {
		return name;
	}

}
