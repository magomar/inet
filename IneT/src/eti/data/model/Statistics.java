package eti.data.model;

/**
 * 
 * @author Mario Gomez-Martinez
 */
public class Statistics {

	/** The Constant FIELDS. */
	public final static String[] FIELDS = { "name", "minValue", "maxValue",
			"average", "stdDev", "median", "missing", "outliers" };

	private String name;
	private double minValue;
	private double maxValue;
	private double mean;
	private double standardDeviation;
	private double median;
	private int missingValues;
	private int outliers;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMinValue() {
		return minValue;
	}

	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}

	public double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}

	public double getMean() {
		return mean;
	}

	public void setMean(double average) {
		this.mean = average;
	}

	public double getStandardDeviation() {
		return standardDeviation;
	}

	public void setStandardDeviation(double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}

	public double getMedian() {
		return median;
	}

	public void setMedian(double median) {
		this.median = median;
	}

	public int getMissingValues() {
		return missingValues;
	}

	public void setMissingValues(int missingValues) {
		this.missingValues = missingValues;
	}

	public int getOutliers() {
		return outliers;
	}

	public void setOutliers(int outliers) {
		this.outliers = outliers;
	}

	/**
	 * Gets an array with the main field values.
	 * 
	 * @return the array
	 */
	public Object[] getArray() {
		Object[] array = { name, minValue, maxValue, mean,
				standardDeviation, median, missingValues, outliers };
		return array;
	}

}
