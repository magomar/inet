package eti.data.tools;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.csvreader.CsvReader;

import eti.data.model.MetaData;

/**
 * @author Mario Gomez-Martinez
 */
public class CSVDataReader {

	private static final Logger logger = Logger.getLogger(CSVDataReader.class
			.getName());

	public static Map<String, List<Double>> readDataFromCSV(String fileToRead) {
		CsvReader csvReader;
		String[] headers = null;
		Map<String, List<Double>> data = new LinkedHashMap<String, List<Double>>();
		try {
			csvReader = new CsvReader(fileToRead, ';');
			csvReader.readHeaders();
			headers = csvReader.getHeaders();
			for (int i = 0; i < headers.length; i++) {
				data.put(headers[i], new ArrayList<Double>());
			}
			while (csvReader.readRecord()) {
				for (int i = 0; i < headers.length; i++) {
					String string = csvReader.get(headers[i]).replace(',', '.');
					double number;
					if (isDouble(string)) {
						number = Double.parseDouble(string);
					} else {
						number = Double.NaN;
					}
					data.get(headers[i]).add(number);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

	public static List<MetaData> readMetaDataFromCSV(String fileToRead) {
		CsvReader csvReader;
		List<MetaData> metadata = new ArrayList<MetaData>();
		try {
			csvReader = new CsvReader(fileToRead, ';');
			csvReader.readHeaders();
			while (csvReader.readRecord()) {
				MetaData varMetaData = new MetaData();
				varMetaData.setName(csvReader.get(0));
				varMetaData.setLabel(csvReader.get(1));
				varMetaData.setMeasure(csvReader.get(2));
				varMetaData.setSet(csvReader.get(3));
				metadata.add(varMetaData);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return metadata;
	}

	/* AUXILIAR METHODS */

	private static boolean isDouble(String string) {
		try {
			Double.parseDouble(string);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

}