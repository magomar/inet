package eti.data.tools;



/**
 * @author Mario Gomez-Martinez
 * 
 */
public class DataNormalizer implements Normalizer {
	private double[][] data;
	private double[][] normalizedData;
	private SimpleArrayStatistics[] stats;
	private final double normalizedMin;
	private final double normalizedMax;

	/**
	 * Creates a new normalizer object to normalize data in the range [0, 1]
	 */
	public DataNormalizer() {
		this(0.0, 1.0);
	}

	/**
	 * Creates a new normalizer object to normalize data in the range
	 * [normalizedMin, normalizedMax]
	 * 
	 * @param normalizedMin
	 * @param normalizedMax
	 */
	public DataNormalizer(double normalizedMin, double normalizedMax) {
		this.normalizedMin = normalizedMin;
		this.normalizedMax = normalizedMax;
	}

	@Override
	public double[][] getData() {
		return data;
	}

	@Override
	public void setData(double[][] data) {
		this.data = data;

		// fixOutliers();
		normalize();
	}

	@Override
	public double[][] getNormalizedData() {
		return normalizedData;
	}

	@Override
	public void setNormalizedData(double[][] normalizedData) {
		this.normalizedData = normalizedData;
		unnormalize();
	}

	private static void unnormalize() {
		// TODO unnormalizeColumns
	}

	/** Normalizes data into the range [normalizedMin, normalizedMax] */
	private void normalize() {
		int rows = data.length;
		int columns = data[0].length;
		normalizedData = new double[rows][columns];
		double newRange = normalizedMax - normalizedMin;

		// double[][] transposed = DataTools.transposeMatrix(data);
		// stats = new SimpleArrayStatistics[transposed.length];
		// for (int j = 0; j < transposed.length; j++) {
		// stats[j] = new SimpleArrayStatistics(transposed[j]);
		// }
		// for (int i = 0; i < rows; i++) {
		// double[] dataRow = data[i];
		// double[] newDataRow = normalizedData[i];
		// for(int j = 0; j < columns; j++) {
		// SimpleArrayStatistics s = stats[j];
		// newDataRow[j] = ((dataRow[j] - s.getMin()) / s.getRange())* newRange
		// + normalizedMin;
		// }
		// }

		double[] min = new double[columns];
		double[] max = new double[columns];
		double[] range = new double[columns];
		// Compute min values, max values, and ranges (max - min) for each
		// column
		for (int j = 0; j < columns; j++) {
			min[j] = data[0][j];
			max[j] = data[0][j];
		}
		for (int i = 1; i < rows; i++) {
			double[] dataRow = data[i];
			for (int j = 0; j < columns; j++) {
				min[j] = Math.min(min[j], dataRow[j]);
				max[j] = Math.max(max[j], dataRow[j]);
			}
		}
		for (int j = 0; j < columns; j++)
			range[j] = max[j] - min[j];
		for (int i = 0; i < rows; i++) {
			double[] dataRow = data[i];
			double[] newDataRow = normalizedData[i];
			for (int j = 0; j < columns; j++) {
				newDataRow[j] = ((dataRow[j] - min[j]) / range[j]) * newRange
						+ normalizedMin;
			}
		}
	}

	/**
	 * Detects elements outside average+- 2* std.deviation
	 */
	private void fixOutliers() {
		double[][] transposed = DataTools.transposeMatrix(data);
		stats = new SimpleArrayStatistics[transposed.length];
		for (int j = 0; j < transposed.length; j++) {
			stats[j] = new SimpleArrayStatistics(transposed[j]);
		}
		int rows = data.length;
		int columns = data[0].length;
		double[] upperBound = new double[columns];
		double[] lowerBound = new double[columns];
		for (int j = 0; j < columns; j++) {
			SimpleArrayStatistics s = stats[j];
			upperBound[j] = s.mean() + 2
					* s.getStandardDeviationForSample();
			lowerBound[j] = s.mean() - 2
					* s.getStandardDeviationForSample();
		}

		for (int i = 0; i < rows; i++) {
			double[] dataRow = data[i];
			for (int j = 0; j < columns; j++) {
				if (dataRow[j] < lowerBound[j])
					dataRow[j] = lowerBound[j];
				else if (dataRow[j] > upperBound[j])
					dataRow[j] = upperBound[j];
			}
		}
	}

	// /** Normalizes data by columns */
	// protected void normalize() {
	// int rows = data.length;
	// int columns = data[0].length;
	// normalizedData = new double[rows][columns];
	// min = new double[columns];
	// max = new double[columns];
	// double[] range = new double[columns];
	// // Compute min values, max values, and ranges (max - min) for each column
	// for(int j = 0; j < columns; j++) {
	// min[j] = data[0][j];
	// max[j] = data[0][j];
	// }
	// for (int i = 1; i < rows; i++)
	// for(int j = 0; j < columns; j++) {
	// min[j] = Math.min(min[j], data[i][j]);
	// max[j] = Math.max(max[j], data[i][j]);
	// }
	// for(int j = 0; j < columns; j++) range[j] = max[j] - min[j];
	// // normalize
	// for (int i = 0; i < rows; i++)
	// for (int j = 0; j < columns; j++) {
	// normalizedData[i][j] = (data[i][j] - min[j]) / range[j];
	// }
	// }
	//

}
