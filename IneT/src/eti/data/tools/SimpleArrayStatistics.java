package eti.data.tools;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public class SimpleArrayStatistics {
	double mean;
	double min = Double.MAX_VALUE;
	double max = Double.MIN_VALUE;;
	double median;
	double standardDeviationForSample;
	double standardDeviationForPopulation;
	double range;
	int missing = 0;

	public SimpleArrayStatistics(double data[]) {
		int count = data.length;
		for (int i = 0; i < count; i++) {
			double datum = data[i];
			if (Double.isNaN(datum))
				missing++;
			else {
				min = Math.min(min, datum);
				max = Math.max(max, datum);
				mean += datum;
			}
		}
		mean /= count;
		range = max - min;
		median = (max + min) / 2;

		double sum = 0;
		for (int i = 0; i < count; i++) {
			double datum = data[i];
			if (!Double.isNaN(datum)) {
				double dif = datum - mean;
				sum += dif * dif;
			}
		}
		standardDeviationForSample = Math.sqrt(sum / (count - 1));
		standardDeviationForPopulation = Math.sqrt(sum / count);
	}

	/*
	 * public SimpleArrayStatistics(List<Double> data) { //
	 * this(Doubles.toArray(data)); // using Guava library
	 * this(DataTools.toDoubleArray(data)); // self-made function }
	 */

	public double mean() {
		return mean;
	}

	public double getMin() {
		return min;
	}

	public double getMax() {
		return max;
	}

	public double getMedian() {
		return median;
	}

	public double getStandardDeviationForSample() {
		return standardDeviationForSample;
	}

	public double getStandardDeviationForPopulation() {
		return standardDeviationForPopulation;
	}

	public double getRange() {
		return range;
	}

	public int getMissing() {
		return missing;
	}
}
