package eti.data.tools;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math.stat.descriptive.SummaryStatistics;

/**
 * @author Mario Gomez-Martinez
 *
 */
public final class DataTools {

	/**
	 * Obtain the different values contained in a column of a 2D array of data
	 * 
	 * @param data
	 * @param aColumn
	 * @return
	 */
	public static double[] getValuesForColumn(double[][] data, int aColumn) {
		double[] values;
		Set<Double> setOfValues = new HashSet<Double>();
		for (int i = 0; i < data.length; i++) {
			setOfValues.add(data[i][aColumn]);
		}
		int numConditions = setOfValues.size();
		values = new double[numConditions];
		int conditionIndex = 0;
		for (Double condition : setOfValues) {
			values[conditionIndex++] = condition.doubleValue();
		}
		return values;
	}

	public static double[][] compareGroups(double[][] inputData,
			double[][] outputData, int controlColumn) {
		double[] conditions = getValuesForColumn(inputData, controlColumn);
		int numRows = inputData.length;
		int numOutputColumns = outputData[0].length;
		Map<Double, SummaryStatistics[]> stats = new HashMap<Double, SummaryStatistics[]>();
		SummaryStatistics[] globalStats = new SummaryStatistics[numOutputColumns];
		double[][] means = new double[conditions.length + 1][numOutputColumns]; // prepare
																				// globalStats
		for (int j = 0; j < numOutputColumns; j++) {
			globalStats[j] = new SummaryStatistics();
		} // prepare stats for every condition
		for (int k = 0; k < conditions.length; k++) {
			SummaryStatistics[] outputStats = new SummaryStatistics[numOutputColumns];
			for (int j = 0; j < numOutputColumns; j++) {
				outputStats[j] = new SummaryStatistics();
			}
			stats.put(conditions[k], outputStats);
		} // add values to stats
		for (int i = 0; i < numRows; i++) {
			double condition = inputData[i][controlColumn];
			SummaryStatistics[] outputStats = stats.get(condition);
			for (int j = 0; j < numOutputColumns; j++) {
				outputStats[j].addValue(outputData[i][j]);
				globalStats[j].addValue(outputData[i][j]);
			}
		} // compute means and show results
		for (int k = 0; k < conditions.length; k++) {
			SummaryStatistics[] outputStats = stats.get(conditions[k]);
			for (int j = 0; j < numOutputColumns; j++) {
				means[k][j] = outputStats[j].getMean();
			}
			System.out.println("Condition: " + conditions[k] + " ==> "
					+ Arrays.toString(means[k]));
		}
		for (int j = 0; j < numOutputColumns; j++) {
			means[conditions.length][j] = globalStats[j].getMean();
		}
		System.out.println("Global results: "
				+ Arrays.toString(means[conditions.length]));
		return means;
	}

	/** returns a copy of a single row in a 2D array */
	public static double[][] singleRow(double[][] data, int row) {
		int numColumns = data[0].length;
		double[][] array = new double[1][numColumns];
		array[0] = Arrays.copyOf(data[row], numColumns);
		return array;
	}

	/** returns a copy of all data except a single row */
	public static double[][] removeRow(double[][] data, int row) {
		int numColumns = data[0].length;
		double[][] array = new double[data.length - 1][numColumns];
		for (int i = 0; i < row; i++)
			array[i] = Arrays.copyOf(data[i], numColumns);
		int newIndex = row;
		for (int i = row + 1; i < data.length; i++)
			array[newIndex++] = Arrays.copyOf(data[i], numColumns);
		return array;
	}

	/**
	 * returns a copy of all data except some rows specified in the list
	 * rowsToRemoce
	 */
	public static double[][] removeSeveralRows(double[][] data,
			Collection<Integer> rowsToRemove) {
		int numColumns = data[0].length;
		double[][] array = new double[data.length - rowsToRemove.size()][numColumns];
		int index = 0;
		for (int i = 0; i < data.length; i++)
			if (!rowsToRemove.contains(i))
				array[index++] = Arrays.copyOf(data[i], numColumns);
		return array;
	}

	/** returns a copy of a number of consecutive rows, starting in firstRow */
	public static double[][] subArray(double[][] data, int firstRow, int numRows) {
		int numColumns = data[0].length;
		double[][] array = new double[numRows][numColumns];
		for (int i = 0; i < numRows; i++)
			array[i] = Arrays.copyOf(data[firstRow + i], numColumns);
		return array;
	}

	/**
	 * returns a copy of all data except a number of consecutive rows starting
	 * in firstRow
	 */
	public static double[][] subArrayInv(double[][] data, int firstRow,
			int numRows) {
		int numColumns = data[0].length;
		double[][] array = new double[data.length - numRows][numColumns];
		for (int i = 0; i < firstRow; i++)
			array[i] = Arrays.copyOf(data[i], numColumns);
		int newIndex = firstRow;
		for (int i = firstRow + numRows; i < data.length; i++)
			array[newIndex++] = Arrays.copyOf(data[i], numColumns);
		return array;
	}

	/**
	 * @param data
	 * @return textual description of array of data
	 */
	public static String toString(double[] data) {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int j = 0; j < data.length; j++) {
			sb.append(String.format("%.2f ", data[j]));
		}
		sb.append("]\n");
		return sb.toString();
	}

	/**
	 * @param data
	 * @return textual description of 2D array of data
	 */
	public static String toString(double[][] data) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < data.length; i++) {
			sb.append(toString(data[i]));
		}
		sb.append("Num Rows: " + data.length + "\n");
		return sb.toString();
	}

	public static double[][] transposeMatrix(double[][] matrix) {
		int numRows = matrix.length;
		int numColumns = matrix[0].length;
		double[][] transposed = new double[numColumns][numRows];
		for (int i = 0; i < numRows; ++i) {
			for (int j = 0; j < numColumns; ++j) {
				transposed[j][i] = matrix[i][j];
			}
		}
		return transposed;
	}

	public static double[] toDoubleArray(List<Double> list) {
		double[] array = new double[list.size()];
		int i = 0;
		for (Double e : list)
			array[i++] = e.doubleValue();
		return array;
	}

	public static int[] toIntArray(List<Integer> list) {
		int[] array = new int[list.size()];
		int i = 0;
		for (Integer e : list)
			array[i++] = e.intValue();
		return array;
	}

}
