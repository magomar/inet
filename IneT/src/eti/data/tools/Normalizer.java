package eti.data.tools;

/**
 * @author Mario Gomez-Martinez
 * 
 * Model for the behaviour of a data normalization instrument. Classes implementing this interface
 * will be able to normalize and unnormalize a 2-dimensional array of double
 */
public interface Normalizer {
	/**
	 * @return the non normalized data
	 */
	public double[][] getData();
	/**
	 * Set new data and obtains a normalized version of the data
	 *  which can be obtained by calling getNormalizedData()
	 * @param data
	 */
	public void setData(double[][] data);
	/**
	 * @return
	 */
	public double[][] getNormalizedData();
	/**
	 * Set new normalized data and obtains a non-normalized version of the data 
	 * which can be obtained by calling getData()
	 * @param normalizedData
	 */
	public void setNormalizedData(double[][] normalizedData);
}
