package eti.data.source;

import eti.data.tools.Normalizer;

/**
 * @author Mario Gomez
 * 
 */
public class NormalizedDataSource extends GenericDataSource {
	Normalizer inputNormalizer;
	Normalizer outputNormalizer;

	// public NormalizedDataSource(DataSource data) {
	// inputNormalizer = new DataNormalizer();
	// inputNormalizer.setData(data.getInputData());
	// this.inputData = inputNormalizer.getNormalizedData();
	// outputNormalizer = new DataNormalizer();
	// outputNormalizer.setData(data.getOutputData());
	// this.outputData = outputNormalizer.getNormalizedData();
	// }

	public NormalizedDataSource(DataSource data, Normalizer inputNormalizer,
			Normalizer outputNormalizer) {
		this.inputNormalizer = inputNormalizer;
		this.inputNormalizer.setData(data.getInputData());
		this.inputData = inputNormalizer.getNormalizedData();
		this.outputNormalizer = outputNormalizer;
		this.outputNormalizer.setData(data.getOutputData());
		this.outputData = outputNormalizer.getNormalizedData();
		this.inputHeaders = data.getInputHeaders();
		this.outputHeaders = data.getOutputHeaders();
	}

	/**
	 * @return the inputNormalizer
	 */
	public Normalizer getInputNormalizer() {
		return inputNormalizer;
	}

	/**
	 * @return the outputNormalizer
	 */
	public Normalizer getOutputNormalizer() {
		return outputNormalizer;
	}

}
