package eti.data.source;


import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import eti.data.model.Experiment;
import eti.data.model.Variable;
import eti.data.tools.DataTools;

public class ExperimentDataSource extends GenericDataSource {
	private final Experiment experiment;
	private static final Logger logger = Logger
			.getLogger(ExperimentDataSource.class.getName());

	public ExperimentDataSource(Experiment experiment) {
		this.experiment = experiment;
		obtainData();
	}

	private void obtainData() {
		List<Variable> inputs = experiment.getInputs();
		List<Variable> outputs = experiment.getOutputs();

		int numRecords = inputs.get(0).getData().length;
		int numInput = inputs.size();
		int numOutput = outputs.size();

		logger.info("Inputs: " + inputs.toString());
		logger.info("Outputs: " + inputs.toString());

		// inputData = new double[numRecords][numInput];
		// outputData = new double[numRecords][numOutput];

		double[][] in = new double[numInput][numRecords];
		double[][] out = new double[numOutput][numRecords];

		// fill data matrices and detect invalid rows (those for which output
		// values are missing)
		int missingValues = 0;
		Set<Integer> invalidRecords = new HashSet<Integer>();

		int i = 0;
		for (Variable var : inputs) {
			var.replaceMissing();
			in[i++] = var.getData();
		}
		i = 0;
		for (Variable var : outputs) {
			invalidRecords.addAll(var.getMissingIndices());
			out[i++] = var.getData();
		}

		inputData = DataTools.transposeMatrix(in);
		outputData = DataTools.transposeMatrix(out);

		// Remove invalid records (records missing some output value)
		inputData = DataTools.removeSeveralRows(inputData, invalidRecords);
		outputData = DataTools.removeSeveralRows(outputData, invalidRecords);
		int totalData = numRecords * (numInput + numOutput);
		logger.info("Num records: " + numRecords);
		logger.info("Total data: " + totalData);
		logger.info("Records removed because output is missing: "
				+ invalidRecords.size());
		int proportionMissing = missingValues * 100 / totalData;
		logger.info("Missing values fixed: " + missingValues + "("
				+ proportionMissing + "%)");

	}

	@Override
	public double[][] getInputData() {
		return inputData;
	}

	@Override
	public double[][] getOutputData() {
		return outputData;
	}

	@Override
	public int getNumRows() {
		return inputData.length;
	}

	@Override
	public int getNumInputColumns() {
		return inputData[0].length;
	}

	@Override
	public int getNumOutputColumns() {
		return outputData[0].length;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Input data\n");
		for (int i = 0; i < inputData.length; i++) {
			sb.append(Arrays.toString(inputData[i]) + "\n");
		}
		sb.append("Output data\n");
		for (int j = 0; j < outputData.length; j++) {
			sb.append(Arrays.toString(outputData[j]) + "\n");

		}
		return sb.toString();
	}

	// private boolean isInteger(String string) {
	//
	// try {
	// Integer.parseInt(string);
	// } catch (NumberFormatException nfe) {
	// return false;
	// }
	// return true;
	// }

	// public boolean isNumber(String string) {
	// Pattern p = Pattern.compile("([0-9]*)\\.[0-9]*");
	// Matcher m = p.matcher(string);
	// return m.matches();
	// }

}
