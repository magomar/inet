package eti.data.source;


import java.util.Arrays;

import eti.solver.ann.ControlVariable;


/**
 * @author Mario Gomez
 * 
 */
public class ControlDataSource extends GenericDataSource {

	/**
	 * @param data
	 * @param controlColumn
	 */
	public ControlDataSource(DataSource data, ControlVariable controlVariable) {
		inputHeaders = data.getInputHeaders();
		outputHeaders = data.getOutputHeaders();
		double[][] originalInputData = data.getInputData();
		double[][] originalOutputData = data.getOutputData();
		int numPatterns = data.getNumRows();
		int numInputColumns = data.getNumInputColumns();
		int numOutputColumns = data.getNumOutputColumns();
		int controlColumn = controlVariable.getControlColumn();
		double[] controlValues = controlVariable.getValues();
		inputData = new double[numPatterns * controlValues.length][numInputColumns];
		outputData = new double[numPatterns * controlValues.length][numOutputColumns];

		int rowIndex = 0;
		for (int i = 0; i < numPatterns; i++) {
			for (int k = 0; k < controlValues.length; k++) {
				for (int j = 0; j < numInputColumns; j++) {
					if (j == controlColumn) {
						inputData[rowIndex][j] = controlValues[k];
					} else {
						inputData[rowIndex][j] = originalInputData[i][j];
					}
				}
//				outputData[rowIndex] = originalOutputData[i].clone();
				outputData[rowIndex] = Arrays.copyOf(originalOutputData[i], numOutputColumns);
				rowIndex++;
			}
		}
	}
}
