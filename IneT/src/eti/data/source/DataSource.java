package eti.data.source;

public interface DataSource {

	public double[][] getInputData();

	public double[][] getOutputData();

	public int getNumRows();

	public int getNumInputColumns();

	public int getNumOutputColumns();

	public String[] getInputHeaders();

	public String[] getOutputHeaders();

}
