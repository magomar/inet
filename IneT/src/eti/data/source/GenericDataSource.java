package eti.data.source;

import java.util.Arrays;

/**
 * @author Mario Gomez
 * 
 */
public class GenericDataSource implements DataSource {
	protected String[] inputHeaders;
	protected String[] outputHeaders;
	protected double[][] inputData;
	protected double[][] outputData;

	public GenericDataSource(double[][] inputData, double[][] outputData,
			String[] inputHeaders, String[] outputHeaders) {
		this.inputData = inputData;
		this.outputData = outputData;
		this.inputHeaders = inputHeaders;
		this.outputHeaders = outputHeaders;
	}

	protected GenericDataSource() {

	}

	@Override
	public double[][] getInputData() {
		return inputData;
	}

	@Override
	public double[][] getOutputData() {
		return outputData;
	}

	@Override
	public int getNumRows() {
		return inputData.length;
	}

	@Override
	public int getNumInputColumns() {
		return inputData[0].length;
	}

	@Override
	public int getNumOutputColumns() {
		return outputData[0].length;
	}

	@Override
	public String[] getInputHeaders() {
		return inputHeaders;
	}

	@Override
	public String[] getOutputHeaders() {
		return outputHeaders;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(Arrays.toString(inputHeaders) + " --> "
				+ Arrays.toString(outputHeaders) + "\n");
		int numRows = getNumRows();
		int numInputColumns = getNumInputColumns();
		int numOutputColumns = getNumOutputColumns();
		for (int i = 0; i < numRows; i++) {
			double[] inputRow = inputData[i];
			double[] outputRow = outputData[i];
			sb.append("[");
			for (int j = 0; j < numInputColumns; j++) {
				sb.append(String.format("%.2f ", inputRow[j]));
			}
			sb.append("] --> [");
			for (int j = 0; j < numOutputColumns; j++) {
				sb.append(String.format("%.2f ", outputRow[j]));
			}
			sb.append("]\n");
		}
		sb.append("Variables: " + getNumInputColumns() + " inputs &  "
				+ getNumOutputColumns() + " outputs" + "\n");
		sb.append("Rows: " + numRows + "\n");
		return sb.toString();
	}

}
