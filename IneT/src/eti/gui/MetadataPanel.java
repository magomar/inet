/**
 * 
 */
package eti.gui;


import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import eti.data.model.DataSet;
import eti.data.model.MetaData;
import eti.data.model.Variable;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public class MetadataPanel extends JScrollPane implements Observer {
	private static final long serialVersionUID = -6665345886154374887L;
	private static final Logger logger = Logger.getLogger(MetadataPanel.class
			.getName());
	private final JTable table;

	public MetadataPanel() {
		DefaultTableModel tableModel = new DefaultTableModel();
		table = new JTable(tableModel);
		setViewportView(table);
		table.setFillsViewportHeight(true);
	}

	@Override
	public void update(Observable o, Object arg) {
		DataSet dataSet = (DataSet) o;
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setColumnCount(0);
		tableModel.setRowCount(0);
		String headers[] = MetaData.FIELDS;
		for (int i = 0; i < headers.length; i++) {
			tableModel.addColumn(headers[i]);
		}
		for (Variable var : dataSet.getVariables().values()) {
			tableModel.addRow(var.getMetaData().getArray());
		}
	}

}
