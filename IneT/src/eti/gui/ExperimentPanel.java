package eti.gui;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import eti.data.model.Experiment;
import eti.data.model.Variable;
import eti.data.tools.DataTools;

public class ExperimentPanel extends JPanel implements Observer {

	private static final long serialVersionUID = -4242664124147547000L;
	private final ScrollableList<Variable> varList;
	private final ScrollableList<Variable> inputList;
	private final ScrollableList<Variable> outputList;
	private final JComboBox<String> setComboBox;
	private final Experiment experiment;

	/**
	 * Create the panel.
	 */
	public ExperimentPanel(Experiment experiment) {
		this.experiment = experiment;

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 150, 0, 150, 0 };
		gridBagLayout.rowHeights = new int[] { 24, 24, 72, 24, 64, 72, 64, 24 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
				0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		setComboBox = new JComboBox<String>();
		GridBagConstraints gbc_setComboBox = new GridBagConstraints();
		gbc_setComboBox.anchor = GridBagConstraints.NORTH;
		gbc_setComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_setComboBox.insets = new Insets(0, 0, 5, 5);
		gbc_setComboBox.gridx = 0;
		gbc_setComboBox.gridy = 0;
		add(setComboBox, gbc_setComboBox);

		JButton btnSelectSet = new JButton("Select set");
		btnSelectSet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String selectedSet = (String) setComboBox.getSelectedItem();
				DefaultListModel<Variable> model = varList.getListModel();
				Object[] array = model.toArray();
				List<Integer> indices = new ArrayList<Integer>();
				for (int i = 0; i < array.length; i++) {
					Variable var = (Variable) array[i];
					if (var.getMetaData().getSet().equals(selectedSet)) {
						indices.add(i);
					}
				}
				varList.getList().setSelectedIndices(
						DataTools.toIntArray(indices));
			}
		});
		GridBagConstraints gbc_btnSelectSet = new GridBagConstraints();
		gbc_btnSelectSet.anchor = GridBagConstraints.NORTH;
		gbc_btnSelectSet.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSelectSet.insets = new Insets(0, 0, 5, 5);
		gbc_btnSelectSet.gridx = 0;
		gbc_btnSelectSet.gridy = 1;
		add(btnSelectSet, gbc_btnSelectSet);

		varList = new ScrollableList<Variable>("Variables",
				experiment.getUnused());
		GridBagConstraints gbc_varList = new GridBagConstraints();
		gbc_varList.fill = GridBagConstraints.BOTH;
		gbc_varList.insets = new Insets(0, 0, 0, 5);
		gbc_varList.gridheight = 5;
		gbc_varList.gridx = 0;
		gbc_varList.gridy = 2;
		add(varList, gbc_varList);

		JButton btnToInputs = new JButton(">");
		btnToInputs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (Variable var : varList.getList().getSelectedValuesList()) {
					inputList.addElement(var);
					varList.removeElement(var);
				}
			}
		});
		GridBagConstraints gbc_btnToInputs = new GridBagConstraints();
		gbc_btnToInputs.anchor = GridBagConstraints.SOUTH;
		gbc_btnToInputs.insets = new Insets(0, 0, 5, 5);
		gbc_btnToInputs.gridx = 1;
		gbc_btnToInputs.gridy = 2;
		add(btnToInputs, gbc_btnToInputs);

		JButton btnInputToVar = new JButton("<");
		btnInputToVar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (Variable var : inputList.getList().getSelectedValuesList()) {
					varList.addElement(var);
					inputList.removeElement(var);
				}
			}
		});
		GridBagConstraints gbc_btnInputToVar = new GridBagConstraints();
		gbc_btnInputToVar.anchor = GridBagConstraints.NORTH;
		gbc_btnInputToVar.insets = new Insets(0, 0, 5, 5);
		gbc_btnInputToVar.gridx = 1;
		gbc_btnInputToVar.gridy = 3;
		add(btnInputToVar, gbc_btnInputToVar);

		JButton btnToOutputs = new JButton(">");
		btnToOutputs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (Variable var : varList.getList().getSelectedValuesList()) {
					outputList.addElement(var);
					varList.removeElement(var);
				}
			}
		});
		GridBagConstraints gbc_btnToOutputs = new GridBagConstraints();
		gbc_btnToOutputs.anchor = GridBagConstraints.SOUTH;
		gbc_btnToOutputs.insets = new Insets(0, 0, 5, 5);
		gbc_btnToOutputs.gridx = 1;
		gbc_btnToOutputs.gridy = 5;
		add(btnToOutputs, gbc_btnToOutputs);

		inputList = new ScrollableList<Variable>("Inputs",
				experiment.getInputs());
		GridBagConstraints gbc_inputList = new GridBagConstraints();
		gbc_inputList.fill = GridBagConstraints.BOTH;
		gbc_inputList.insets = new Insets(0, 0, 5, 0);
		gbc_inputList.gridheight = 3;
		gbc_inputList.gridx = 2;
		gbc_inputList.gridy = 2;
		add(inputList, gbc_inputList);

		outputList = new ScrollableList<Variable>("Outputs",
				experiment.getOutputs());
		GridBagConstraints gbc_ouputList = new GridBagConstraints();
		gbc_ouputList.gridheight = 2;
		gbc_ouputList.fill = GridBagConstraints.BOTH;
		gbc_ouputList.gridx = 2;
		gbc_ouputList.gridy = 5;
		add(outputList, gbc_ouputList);

		DefaultComboBoxModel<String> comboBoxModel = new DefaultComboBoxModel<String>();
		setComboBox.setModel(comboBoxModel);

		JButton btnOutputToVar = new JButton("<");
		btnOutputToVar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (Variable var : outputList.getList()
						.getSelectedValuesList()) {
					varList.addElement(var);
					outputList.removeElement(var);
				}
			}
		});
		GridBagConstraints gbc_btnOutputToVar = new GridBagConstraints();
		gbc_btnOutputToVar.anchor = GridBagConstraints.NORTH;
		gbc_btnOutputToVar.insets = new Insets(0, 0, 0, 5);
		gbc_btnOutputToVar.gridx = 1;
		gbc_btnOutputToVar.gridy = 6;
		add(btnOutputToVar, gbc_btnOutputToVar);

	}

	@Override
	public void update(Observable o, Object arg) {
		experiment.clearRoles();
		varList.update();
		inputList.update();
		outputList.update();
		setComboBox.removeAllItems();
		DefaultComboBoxModel<String> comboBoxModel = (DefaultComboBoxModel<String>) setComboBox
				.getModel();
		for (Variable var : experiment.getDataSet().getVariables().values()) {
			String set = var.getMetaData().getSet();
			if (comboBoxModel.getIndexOf(set) == -1)
				comboBoxModel.addElement(set);
		}

	}
}
