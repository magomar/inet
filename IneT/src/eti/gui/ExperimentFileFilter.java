package eti.gui;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * @author Mario Gomez
 * 
 */
public class ExperimentFileFilter extends FileFilter {

	@Override
	public boolean accept(File f) {
		if (getExtension(f).equals("xml"))
			return true;
		else return false;
	}

	@Override
	public String getDescription() {
		return "XML";
	}

	private String getExtension(File f) {
		if (f != null) {
			String filename = f.getName();
			int i = filename.lastIndexOf('.');
			if (i > 0 && i < filename.length() - 1) {
				return filename.substring(i + 1).toLowerCase();
			}
			;
		}
		return null;
	}

}
