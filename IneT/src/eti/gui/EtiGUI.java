package eti.gui;


import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import eti.data.model.DataSet;
import eti.data.model.Experiment;
import eti.data.tools.CSVDataReader;
import eti.solver.ann.tasks.TrainAndControlNetwork;
import eti.solver.ann.tasks.TrainAndValidateNetwork;
import eti.solver.ann.tasks.TrainSingleNetwork;

/**
 * @author Mario Gomez-Martinez
 */
@SuppressWarnings("serial")
public class EtiGUI {

	private static final Logger logger = Logger.getLogger(EtiGUI.class
			.getName());

	private JFrame frame;

	private final Action openAction = new OpenAction();
	private final Action openMetaDataAction = new OpenMetadataAction();
	private final Action saveExperimentAction = new SaveExperimentAction();
	private final Action loadAction = new LoadAction();
	private final Action trainAction = new TrainAction();
	private final Action validateAction = new ValidateAction();
	private final Action controlAction = new ControlAction();
	private JTabbedPane tabbedPane;

	private final Experiment experiment;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					EtiGUI window = new EtiGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EtiGUI() {
		initialize();
		experiment = new Experiment();
		DataSet dataSet = experiment.getDataSet();
		DataPanel dataPanel = new DataPanel();
		dataSet.addObserver(dataPanel);
		tabbedPane.addTab("Data", dataPanel);
		MetadataPanel metadataPanel = new MetadataPanel();
		tabbedPane.addTab("Metadata", metadataPanel);
		dataSet.addObserver(metadataPanel);
		ExperimentPanel experimentPanel = new ExperimentPanel(experiment);
		tabbedPane.addTab("Experiments", experimentPanel);
		dataSet.addObserver(experimentPanel);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("eTI Data Explorer");
		frame.setBounds(0, 0, 800, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		mnFile.add(openAction);
		mnFile.add(openMetaDataAction);
		mnFile.add(saveExperimentAction);
		mnFile.add(loadAction);

		JMenu mnTasks = new JMenu("Tasks");
		menuBar.add(mnTasks);

		mnTasks.add(trainAction);
		mnTasks.add(validateAction);
		mnTasks.add(controlAction);

		JTextArea logArea = new JTextArea();
		logArea.setEditable(false);
		JScrollPane logPane = new JScrollPane(logArea);
		frame.getContentPane().add(logPane, BorderLayout.SOUTH);

		JToolBar toolBar = new JToolBar();
		frame.getContentPane().add(toolBar, BorderLayout.NORTH);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);

	}

	private class OpenAction extends AbstractAction {

		public OpenAction() {
			putValue(ACCELERATOR_KEY,
					KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
			putValue(NAME, "Open");
			putValue(SHORT_DESCRIPTION, "Open CSV data file");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new File(System.getProperty("user.dir")
					+ "\\data"));
			chooser.setDialogTitle("Select data file (csv)");
			chooser.setFileSelectionMode(JFileChooser.OPEN_DIALOG);
			chooser.setAcceptAllFileFilterUsed(true);
			chooser.setFileFilter(new CSVFileFilter());
			if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				experiment.getDataSet().setData(
						CSVDataReader.readDataFromCSV(file.getAbsolutePath()));

			} else
				logger.warning("File not selected");
		}
	}

	private class OpenMetadataAction extends AbstractAction {

		public OpenMetadataAction() {
			putValue(ACCELERATOR_KEY,
					KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
			putValue(NAME, "Open info");
			putValue(SHORT_DESCRIPTION, "Open CSV Variable view (from SPSS)");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new File(System.getProperty("user.dir")
					+ "\\data"));
			chooser.setDialogTitle("Select metadata file (csv) to open");
			chooser.setFileSelectionMode(JFileChooser.OPEN_DIALOG);
			chooser.setAcceptAllFileFilterUsed(true);
			chooser.setFileFilter(new CSVFileFilter());
			if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				experiment.getDataSet().setMetadata(
						CSVDataReader.readMetaDataFromCSV(file
								.getAbsolutePath()));
			} else
				logger.warning("File not selected");
		}
	}

	private class SaveExperimentAction extends AbstractAction {

		public SaveExperimentAction() {
			putValue(ACCELERATOR_KEY,
					KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
			putValue(NAME, "Save experiment");
			putValue(SHORT_DESCRIPTION, "Save experiment)");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new File(System.getProperty("user.dir")
					+ "\\data"));
			chooser.setDialogTitle("Select file to save experiment");
			chooser.setFileSelectionMode(JFileChooser.SAVE_DIALOG);
			chooser.setAcceptAllFileFilterUsed(true);
			chooser.setFileFilter(new CSVFileFilter());
			if (chooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {

			} else
				logger.warning("File not selected");
		}
	}

	private class LoadAction extends AbstractAction {

		public LoadAction() {
			putValue(ACCELERATOR_KEY,
					KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
			putValue(NAME, "Load");
			putValue(SHORT_DESCRIPTION, "Load experiment");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// JFileChooser chooser = new JFileChooser();
			// chooser.setCurrentDirectory(new
			// File(System.getProperty("user.dir")
			// + "\\data"));
			// chooser.setDialogTitle("Select experiment description file");
			// chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			// chooser.setAcceptAllFileFilterUsed(true);
			// chooser.setFileFilter(new ExperimentFileFilter());
			// if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION)
			// {
			// File file = chooser.getSelectedFile();
			// eti.model.experiment.Experiment experiment = DataTools
			// .loadExperiment(file.getAbsolutePath());
			// taskLauncher = new TaskLauncher();
			// taskLauncher.setExperiment(experiment);
			//
			// } else
			// logger.warning("File not selected");
		}
	}

	private class TrainAction extends AbstractAction {

		public TrainAction() {
			putValue(ACCELERATOR_KEY,
					KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
			putValue(NAME, "Train");
			putValue(SHORT_DESCRIPTION, "Train neural network");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			TrainSingleNetwork task = new TrainSingleNetwork(experiment);
			task.run();
		}
	}

	private class ValidateAction extends AbstractAction {

		public ValidateAction() {
			putValue(ACCELERATOR_KEY,
					KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
			putValue(NAME, "Validate");
			putValue(SHORT_DESCRIPTION, "Validate neural network");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			TrainAndValidateNetwork task = new TrainAndValidateNetwork(
					experiment);
			task.run();
		}
	}

	private class ControlAction extends AbstractAction {
		public ControlAction() {
			putValue(ACCELERATOR_KEY,
					KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
			putValue(NAME, "Control");
			putValue(SHORT_DESCRIPTION, "Control neural network");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			TrainAndControlNetwork task = new TrainAndControlNetwork(experiment);
			task.run();
		}
	}
}
