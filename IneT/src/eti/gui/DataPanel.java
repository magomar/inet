package eti.gui;


import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import eti.data.model.DataSet;
import eti.data.model.Statistics;
import eti.data.model.Variable;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public class DataPanel extends JScrollPane implements Observer {
	private static final long serialVersionUID = -8761879320147743080L;
	private static final Logger logger = Logger.getLogger(DataPanel.class
			.getName());
	private final JTable table;

	public DataPanel() {
		DefaultTableModel tableModel = new DefaultTableModel();
		table = new JTable(tableModel);
		setViewportView(table);
		table.setFillsViewportHeight(true);
		table.setEnabled(false);
	}

	@Override
	public void update(Observable o, Object arg) {
		DataSet dataSet = (DataSet) o;
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setColumnCount(0);
		tableModel.setRowCount(0);
		String headers[] = Statistics.FIELDS;
		for (int i = 0; i < headers.length; i++) {
			tableModel.addColumn(headers[i]);
		}
		for (Variable var : dataSet.getVariables().values()) {
			if (var.getData() != null)
				tableModel.addRow(var.getStatistics().getArray());
		}

	}
}
