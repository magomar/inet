package eti.gui;

import java.awt.Color;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

public class ScrollableList<E> extends JScrollPane {

	private static final long serialVersionUID = -3192409417315718871L;
	private final DefaultListModel<E> listModel = new DefaultListModel<E>();
	private final JList<E> jList;
	private final List<E> list;

	public ScrollableList(String title, List<E> list) {
		super();

		JLabel lblTitle = new JLabel(title);
		lblTitle.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		setColumnHeaderView(lblTitle);

		jList = new JList<E>(listModel);
		setViewportView(jList);
		this.list = list;
		update();
	}

	public void addElement(E e) {
		listModel.addElement(e);
		list.add(e);
	}

	public void removeElement(E e) {
		listModel.removeElement(e);
		list.remove(e);
	}

	// public void clear() {
	// listModel.clear();
	// list.clear();
	// }

	public DefaultListModel<E> getListModel() {
		return listModel;
	}

	public JList<E> getList() {
		return jList;
	}

	public void update() {
		listModel.clear();
		for (E element : list) {
			listModel.addElement(element);
		}
	}
}
