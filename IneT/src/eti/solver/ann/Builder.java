package eti.solver.ann;

import org.joone.net.NeuralNet;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public interface Builder {
	NeuralNet getNnet();
}
