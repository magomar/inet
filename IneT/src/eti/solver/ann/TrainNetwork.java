package eti.solver.ann;


import org.joone.engine.Monitor;
import org.joone.engine.NeuralNetEvent;
import org.joone.net.NeuralNet;


/**
 * @author Mario Gomez-Martinez
 * 
 */
public class TrainNetwork implements Trainer {
	protected NeuralNet net;

	public TrainNetwork(NeuralNet nnet) {
		this(nnet, new TrainingParameters());
	}

	public TrainNetwork(NeuralNet nnet, TrainingParameters trainingParameters) {
		this.net = NeuralNetworkTools.cloneNet(nnet);
		// Get the Monitor object and set the learning parameters
		Monitor monitor = this.net.getMonitor();
		trainingParameters.setTrainingParameters(monitor);
	}

	@Override
	public NeuralNet call() throws Exception {
		return train();
	}
	
	@Override
	public NeuralNet train() {
		Monitor monitor = net.getMonitor();
		monitor.addNeuralNetListener(this);
		monitor.setLearning(true); // The net must be trained
		net.go(true); // The net starts the training job
		return net;
	}
	
	/* NEURAL NET LISTENER METHODS */

	@Override
	public void cicleTerminated(NeuralNetEvent arg0) {
	}

	@Override
	public void errorChanged(NeuralNetEvent e) {
		Monitor monitor = (Monitor) e.getSource();
		int remainingCycles = monitor.getCurrentCicle();
//		if (remainingCycles % 1000 == 0)	System.out.print("*");
//		else if (remainingCycles % 100 == 0)	System.out.print(".");
		// print RMSE every 1000 cycles
		if (remainingCycles % 1000 == 0)	
			print(remainingCycles +
					" cycles remaining - RMSE = " + monitor.getGlobalError());
	}

	@Override
	public void netStarted(NeuralNetEvent e) {
		print("Training started");
	}

	@Override
	public void netStopped(NeuralNetEvent e) {
		System.out.println();
		Monitor monitor = (Monitor) e.getSource();
		print("Training finished. RMSE =  " + monitor.getGlobalError());
	}

	@Override
	public void netStoppedError(NeuralNetEvent e, String error) {
		print(" ERROR !  " + error);
		System.exit(1);
	}

	/* AUXILIAR METHODS */
	
	protected void print(String msg) {
		System.out.println(net.getDescriptor().getNeuralNetName() + ": " + msg);
	}
    
}
