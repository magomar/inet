package eti.solver.ann;

import java.util.concurrent.Callable;

import org.joone.engine.NeuralNetListener;
import org.joone.net.NeuralNet;

/**
 * @author Mario Gomez-Martinez
 *
 */
public interface Trainer extends Callable<NeuralNet>, NeuralNetListener {
	public NeuralNet train();
}
