package eti.solver.ann.tasks;


import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.math.stat.descriptive.SummaryStatistics;
import org.joone.net.NeuralNet;

import eti.data.model.Experiment;
import eti.data.source.DataSource;
import eti.data.source.GenericDataSource;
import eti.data.tools.DataTools;
import eti.solver.ann.BuildNetwork;
import eti.solver.ann.Builder;
import eti.solver.ann.TrainNetwork;
import eti.solver.ann.ValidateNetwork;
import eti.solver.ann.ValidationResults;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public class TrainAndValidateNetwork extends TrainSingleNetwork {

	/**
	 * @param experimentFilename
	 */
	public TrainAndValidateNetwork(Experiment experiment) {
		super(experiment);
	}

	@Override
	public void run() {
		Runtime runtime = Runtime.getRuntime();
		int nrOfProcessors = runtime.availableProcessors();
		ExecutorService threadPool = Executors
				.newFixedThreadPool(nrOfProcessors);
		CompletionService<NeuralNet> trainingPool = new ExecutorCompletionService<NeuralNet>(
				threadPool);
		CompletionService<ValidationResults> validationResultsPool = new ExecutorCompletionService<ValidationResults>(
				threadPool);
		int numPatterns = normalizedData.getNumRows();
		SummaryStatistics stats = new SummaryStatistics();
		for (int i = 0; i < numPatterns; i++) {
			DataSource newDataSource = new GenericDataSource(
					DataTools.removeRow(normalizedData.getInputData(), i),
					DataTools.removeRow(normalizedData.getOutputData(), i),
					normalizedData.getInputHeaders(),
					normalizedData.getOutputHeaders());

			Builder newBuilder = new BuildNetwork("N" + i, newDataSource);
			trainingPool.submit(new TrainNetwork(newBuilder.getNnet()));
		}
		for (int i = 0; i < numPatterns; i++) {
			try {
				NeuralNet trainedNet = trainingPool.take().get();
				String name = trainedNet.getDescriptor().getNeuralNetName();
				int index = Integer.parseInt(name.substring(name.length() - 1));
				// System.out.println("Network training finished " + name
				// + ", " + index);
				DataSource validationDataSource = new GenericDataSource(
						DataTools.singleRow(normalizedData.getInputData(),
								index), DataTools.singleRow(
								normalizedData.getOutputData(), index),
						normalizedData.getInputHeaders(),
						normalizedData.getOutputHeaders());
				// System.out.println(Arrays.toString(validationDataSource
				// .getInputData()[0]));
				// System.out.println(Arrays.toString(validationDataSource
				// .getOutputData()[0]));
				validationResultsPool.submit(new ValidateNetwork(trainedNet,
						validationDataSource));

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		for (int i = 0; i < numPatterns; i++) {
			ValidationResults results;
			try {
				results = validationResultsPool.take().get();
				stats.addValue(results.getRootMeanSquaredError());
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		System.out.println("AVERAGE Validation Error: " + stats.getMean());
		threadPool.shutdown();

	}

}
