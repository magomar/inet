package eti.solver.ann.tasks;


import java.util.logging.Logger;

import org.joone.net.NeuralNet;

import eti.data.model.Experiment;
import eti.data.source.DataSource;
import eti.data.source.ExperimentDataSource;
import eti.data.source.NormalizedDataSource;
import eti.data.tools.DataNormalizer;
import eti.data.tools.Normalizer;
import eti.solver.ann.BuildNetwork;
import eti.solver.ann.Builder;
import eti.solver.ann.TrainNetwork;
import eti.solver.ann.Trainer;
import eti.solver.ann.ValidateNetwork;
import eti.solver.ann.ValidationResults;
import eti.solver.ann.Validator;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public class TrainSingleNetwork implements Runnable {
	private static final Logger logger = Logger
			.getLogger(TrainSingleNetwork.class.getName());
	protected DataSource normalizedData;

	public TrainSingleNetwork(Experiment experiment) {
		DataSource data = new ExperimentDataSource(experiment);
		Normalizer inputDataNormalizer = new DataNormalizer(-0.9, 0.9);
		Normalizer outputDataNormalizer = new DataNormalizer(0.1, 0.9);
		normalizedData = new NormalizedDataSource(data, inputDataNormalizer,
				outputDataNormalizer);
		logger.info(normalizedData.toString());
	}

	@Override
	public void run() {
		Builder builder = new BuildNetwork("N1", normalizedData);
		Trainer trainer = new TrainNetwork(builder.getNnet());
		NeuralNet trainedNet = trainer.train();
		Validator validator = new ValidateNetwork(trainedNet, normalizedData);
		ValidationResults results = validator.validate();
		// System.out.println(results);

	}

}
