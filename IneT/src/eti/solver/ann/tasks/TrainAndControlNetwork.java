package eti.solver.ann.tasks;

import eti.data.source.DataSource;
import eti.solver.ann.ControlVariable;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public class TrainAndControlNetwork extends TrainSingleNetwork {
	public TrainAndControlNetwork(eti.data.model.Experiment experiment) {
		super(experiment);
		// TODO Auto-generated constructor stub
	}

	protected DataSource expandedData;
	protected ControlVariable[] controlVariable;

	/*
	 * public TrainAndControlNetwork(Experiment experiment) { super(experiment);
	 * findControlColumns(experiment); }
	 * 
	 * private void findControlColumns(Experiment experiment) { Metadata
	 * metadata = experiment.getMetadata(); Variable[] variable =
	 * (metadata.getVariables()).getVariable(); List<String> controlVariableList
	 * = new ArrayList<String>(); for (int j = 0; j < variable.length; j++) { if
	 * (variable[j].getControl() != null) {
	 * controlVariableList.add(variable[j].getName()); } }
	 * 
	 * this.controlVariable = new ControlVariable[controlVariableList.size()];
	 * 
	 * String[] inputHeaders = normalizedData.getInputHeaders(); int columnIndex
	 * = 0; for (int j = 0; j < inputHeaders.length; j++) { if
	 * (controlVariableList.contains(inputHeaders[j])) {
	 * this.controlVariable[columnIndex] = new ControlVariable( inputHeaders[j],
	 * j, DataTools.getValuesForColumn( normalizedData.getInputData(), j)); } }
	 * Arrays.sort(this.controlVariable); }
	 * 
	 * @Override public void run() { // System.out.println(expandedData);
	 * System.out.println("\nINICIO DE CONTROL: \n"); for (int j = 0; j <
	 * controlVariable.length; j++) { Builder builder = new BuildNetwork("N1",
	 * normalizedData); Trainer trainer = new TrainNetwork(builder.getNnet());
	 * expandedData = new ControlDataSource(normalizedData, controlVariable[j]);
	 * int controlColumn = controlVariable[j].getControlColumn();
	 * System.out.println(" INICIO DE SUBCONTROL: " +
	 * controlVariable[j].getName());
	 * DataTools.compareGroups(normalizedData.getInputData(),
	 * normalizedData.getOutputData(), controlColumn); NeuralNet trainedNet =
	 * trainer.train(); Solver solver = new SolveNetwork(trainedNet,
	 * expandedData.getInputData()); double[][] outputResults = solver.solve();
	 * // DataTools.compareGroups(expandedData.getInputData(), // outputResults,
	 * controlColumn); Solution solution = new
	 * Solution(normalizedData.getInputData(), expandedData.getInputData(),
	 * outputResults, controlVariable[j]); solution.analyse();
	 * System.out.println(" *** FIN DE SUBCONTROL: " +
	 * expandedData.getInputHeaders()[controlColumn]); }
	 * System.out.println("FIN DE CONTROL"); }
	 */
}
