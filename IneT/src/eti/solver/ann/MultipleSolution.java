package eti.solver.ann;

/**
 * 
 * A single solution is an array of double, where each element represents a
 * particular value to assign to a control variable (eg. the type of treatment
 * to apply to a patient)
 * 
 * @author Mario Gomez-Martinez
 * 
 */
public class MultipleSolution {
	private SingleSolution[] ss;
	private int bestConditionIndex;
	private SolutionEvaluationFunction evaluationFunction;

	/**
	 * @param outputData
	 */
	public MultipleSolution(SingleSolution[] ss, ControlVariable controlVariable, SolutionEvaluationFunction evaluationFunction) {
		this.ss = ss;
		this.evaluationFunction = evaluationFunction;
		bestConditionIndex = evaluationFunction.getBestSolution(this).getConditionIndex();
	}

	public SingleSolution getBestSolution() {
		return ss[bestConditionIndex];
	}

	/**
	 * @return the bestConditionIndex
	 */
	public int getBestConditionIndex() {
		return bestConditionIndex;
	}

	@Override
	public String toString() {
		return "#" + bestConditionIndex;
	}

	/**
	 * @return the solutions
	 */
	public SingleSolution[] getSolutions() {
		return ss;
	}
}
