package eti.solver.ann;

/**
 * @author Mario Gomez-Martinez
 *
 */
public class ControlVariable implements Comparable<ControlVariable>{
	private String name;
	private int controlColumn;
	private double[] values;
	
	public ControlVariable(String name, int controlColumn, double[] values) {
		this.name = name;
		this.controlColumn = controlColumn;
		this.values = values;
	}
	public String getName() {
		return name;
	}
	public int getControlColumn() {
		return controlColumn;
	}
	
	
	public double[] getValues() {
		return values;
	}

	@Override
	public int compareTo(ControlVariable arg0) {
		return this.controlColumn - arg0.controlColumn;
	}
	
}
