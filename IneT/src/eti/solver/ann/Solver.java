/**
 * 
 */
package eti.solver.ann;

import java.util.concurrent.Callable;

import org.joone.engine.NeuralNetListener;

/**
 * @author Mario Gomez-Martinez
 *
 */
public interface Solver extends Callable<double[][]>, NeuralNetListener {
	
	public double[][] solve();
}
