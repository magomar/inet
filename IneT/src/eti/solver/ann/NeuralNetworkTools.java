package eti.solver.ann;

import org.joone.engine.Layer;
import org.joone.engine.Synapse;
import org.joone.io.MemoryInputSynapse;
import org.joone.io.StreamInputSynapse;
import org.joone.net.NeuralNet;
import org.joone.util.LearningSwitch;
import org.joone.util.NormalizerPlugIn;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public final class NeuralNetworkTools {

	/** Creates a FileInputSynapse */
	public static MemoryInputSynapse createInput(String name, double[][] data) {
		MemoryInputSynapse input = new MemoryInputSynapse();
		input.setName(name);
		input.setInputArray(data);
		if (data[0].length > 1)
			input.setAdvancedColumnSelector("1-" + data[0].length);
		else
			input.setAdvancedColumnSelector("1");
		return input;
	}

	/** Creates a FileInputSynapse normalizing data in the range [min, max] */
	public static MemoryInputSynapse createNormalizedInput(String name,
			double[][] data, double min, double max) {
		MemoryInputSynapse input = new MemoryInputSynapse();
		input.setName(name);
		input.setInputArray(data);
		if (data[0].length > 1)
			input.setAdvancedColumnSelector("1-" + data[0].length);
		else
			input.setAdvancedColumnSelector("1");

		// We normalize the input data in the range [min,max]
		NormalizerPlugIn norm = new NormalizerPlugIn();
		norm.setName("Normalizer" + name);
		if (data[0].length > 0)
			norm.setAdvancedSerieSelector("1-" + data[0].length);
		else
			norm.setAdvancedSerieSelector("1");
		norm.setMin(min);
		norm.setMax(max);
		input.addPlugIn(norm);
		return input;
	}

	/**
	 * Creates a FileInputSynapse using selected rows, normalizing data in the
	 * range [min, max]
	 */
	public static MemoryInputSynapse createNormalizedInput(String name,
			double[][] data, int firstRow, int lastRow, double min, double max) {
		MemoryInputSynapse input = new MemoryInputSynapse();
		input.setName(name);
		input.setInputArray(data);
		input.setFirstRow(firstRow + 1);
		input.setLastRow(lastRow + 1);
		if (data[0].length > 1)
			input.setAdvancedColumnSelector("1-" + data[0].length);
		else
			input.setAdvancedColumnSelector("1");

		// We normalize the input data in the range [min,max]
		NormalizerPlugIn norm = new NormalizerPlugIn();
		norm.setName("Normalizer" + name);
		if (data[0].length > 0)
			norm.setAdvancedSerieSelector("1-" + data[0].length);
		else
			norm.setAdvancedSerieSelector("1");
		norm.setMin(min);
		norm.setMax(max);
		input.addPlugIn(norm);
		return input;
	}

	/** Connects two Layers with a Synapse **/
	public static void connectLayers(Layer ly1, Synapse syn, Layer ly2) {
		ly1.addOutputSynapse(syn);
		ly2.addInputSynapse(syn);
	}

	/**
	 * Creates a LearningSwitch and attach to it both the training and the
	 * desired input synapses
	 */
	public static LearningSwitch createSwitch(String name,
			StreamInputSynapse syn1, StreamInputSynapse syn2) {
		LearningSwitch lsw = new LearningSwitch();
		lsw.setName(name);
		lsw.addTrainingSet(syn1);
		lsw.addValidationSet(syn2);
		return lsw;
	}

	/**
	 * Clones the neural network passed as parameter. It is achieving by copying
	 * the original network and then removing all listeners to avoid calling any
	 * method of previously registered listeners
	 */
	public static NeuralNet cloneNet(NeuralNet net) {
		// Creates a copy of the neural network
		net.getMonitor().setExporting(true);
		NeuralNet newNet = net.cloneNet();
		net.getMonitor().setExporting(false);

		// Cleans the old listeners
		// This is a fundamental action to avoid that the validated net
		// calls any method of previously registered listeners
		newNet.removeAllListeners();
		return newNet;
	}
}
