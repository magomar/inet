package eti.solver.ann;

import java.util.Arrays;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public class Solution {
	private MultipleSolution[] solutions;
	private double[][] originalInputData;
	private ControlVariable controlVariable;

	public Solution(double[][] originalInputData, double[][] inputData, double[][] outputData, ControlVariable controlVariable) {
		this.originalInputData = originalInputData;
		this.controlVariable = controlVariable;
//		int numPatterns = inputData.length / conditions.length;
		int numPatterns = originalInputData.length;
//		int numInputColumns = inputData[0].length;
//		int numOutputColumns = outputData[0].length;
		solutions = new MultipleSolution[numPatterns];
		int numControlValues = controlVariable.getValues().length;
		int rowIndex = 0;
		for (int i = 0; i < numPatterns; i++) {
			SingleSolution[] ss = new SingleSolution[numControlValues];
			for (int k = 0; k < numControlValues; k++) {
				ss[k] = new SingleSolution(outputData[rowIndex], k);
				rowIndex++;
			}
			// TODO check evaluation functions, not sure if this was finished or not
			solutions[i] = new MultipleSolution(ss, controlVariable, new ProportionalEvaluationFunction());
		}
	}

	public void analyse() {
		int numOutputColumns = solutions[0].getBestSolution().getOutputData().length;
		int numPatterns = solutions.length;
		double globalMean = 0.0;
		double[] means = new double[numOutputColumns];

		for (int i = 0; i < numPatterns; i++) {
			globalMean += solutions[i].getBestSolution().getOutputValue();
			double[] bestOutputData =  solutions[i].getBestSolution().getOutputData();
			for (int j = 0; j < numOutputColumns; j++) {
				means[j] += bestOutputData[j];
			}
		}
		for (int j = 0; j < numOutputColumns; j++) {
			means[j] /= numPatterns;
		}
		globalMean /= numPatterns;
		System.out.println("Average output values: " + Arrays.toString(means));
		System.out.println("Average aggregated output values: " + globalMean);
//		System.out.println(Arrays.toString(solutions));
		double[] controlValues = controlVariable.getValues();
		int[] conditionCounter = new int[controlValues.length];
		int numDiscrepancies = 0;
		for (int i = 0; i < numPatterns; i++) {
			int conditionIndex = solutions[i].getBestConditionIndex();
			conditionCounter[conditionIndex]++;
			if (originalInputData[i][controlVariable.getControlColumn()] != controlValues[conditionIndex]) numDiscrepancies++;
		}
		System.out.println("Frecuencia de las mejores condiciones de control");
		for (int k = 0; k < controlValues.length; k++) {
			System.out.println("Condition " + controlValues[k] + ": " + conditionCounter[k]);
		}
		System.out.println("Discrepancias con los datos originales: " + numDiscrepancies);
	}
}
