package eti.solver.ann;

import java.util.Arrays;

/**
 * @author Mario Gomez-Martinez
 *
 */
public class ValidationResults {
	private double[][] output;
	private double[][] target;
	private int currentPattern;
	private double rootMeanSquaredError;
	private boolean errorComputed;

	public ValidationResults(int numPatterns, int numOutputColumns) {
		output = new double[numPatterns][numOutputColumns];
		target = new double[numPatterns][numOutputColumns];
		errorComputed = false;
	}

	public void addPattern(double[] outputPattern, double[] targetPattern) {
		this.output[currentPattern] = outputPattern;
		this.target[currentPattern] = targetPattern;
		currentPattern++;
	}

	// public void addPartialData(double[][] partialOutput,
	// double[][] partialTarget) {
	// for (int i = 0; i < output.length; i++) {
	// output[currentPattern++] = partialOutput[i];
	// target[currentPattern++] = partialTarget[i];
	// }
	// }

	private void rootMeanSquaredError() {
		double globalError = 0;
		for (int i = 0; i < output.length; i++) {
			double patternError = 0;
			double[] outputPattern = output[i];
			double[] targetPattern = target[i];
			for (int j = 0; j < outputPattern.length; j++) {
				double error = outputPattern[j] - targetPattern[j];
				patternError += error * error;
			}
			globalError += patternError;
		}
		globalError /= output.length;
		rootMeanSquaredError = Math.sqrt(globalError);
		errorComputed = true;
	}

	public double getRootMeanSquaredError() {
		if (!errorComputed)
			rootMeanSquaredError();
		return rootMeanSquaredError;
	}
	
	/**
	 * @return the output
	 */
	public double[][] getOutput() {
		return output;
	}
	
	/**
	 * @return the target
	 */
	public double[][] getTarget() {
		return target;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int numPatterns = output.length;
		for (int i = 0; i < numPatterns; i++) {
			double[] outputPattern = output[i];
			double[] targetPattern = target[i];
			sb.append("Pattern #" + i + " O = " + Arrays.toString(outputPattern)
					+ " T =" + Arrays.toString(targetPattern) + "\n");
		
		}
	return sb.toString();
	}
	
}
