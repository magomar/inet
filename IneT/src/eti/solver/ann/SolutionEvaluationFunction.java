/**
 * 
 */
package eti.solver.ann;

/**
 * @author Mario Gomez-Martinez
 *
 */
public interface SolutionEvaluationFunction {
	public SingleSolution getBestSolution(MultipleSolution ms);

}
