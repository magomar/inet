package eti.solver.ann;


import org.joone.engine.Layer;
import org.joone.engine.Monitor;
import org.joone.engine.NeuralNetEvent;
import org.joone.io.MemoryInputSynapse;
import org.joone.io.MemoryOutputSynapse;
import org.joone.net.NeuralNet;

import eti.data.source.DataSource;


/**
 * @author Mario Gomez-Martinez
 *
 */
public class ValidateNetwork implements Validator {
	protected NeuralNet net;
	protected DataSource validationData;

	public ValidateNetwork(NeuralNet nnet, DataSource validationData) {
		this.net = NeuralNetworkTools.cloneNet(nnet);
		this.validationData = validationData;

	}

	@Override
	public ValidationResults call() throws Exception {
		return validate();
	}

	@Override
	public ValidationResults validate() {
		/* INPUT */
		Layer inputLayer = net.getInputLayer();
		inputLayer.removeAllInputs();
		MemoryInputSynapse inputSynapse = NeuralNetworkTools.createInput(
				"Input Synapse", validationData.getInputData());
		inputLayer.addInputSynapse(inputSynapse);

		/* OUTPUT */
		Layer outputLayer = net.getOutputLayer();
		outputLayer.removeAllOutputs();
		MemoryOutputSynapse outputSynapse = new MemoryOutputSynapse();
		outputLayer.addOutputSynapse(outputSynapse);

		/* VALIDATION */
		// Get the Monitor object and set the learning parameters
		Monitor monitor = this.net.getMonitor();
		monitor.setTrainingPatterns(validationData.getNumRows());
		monitor.setLearning(false);
		monitor.setTotCicles(1);

		net.go(true);

		// CARE !! validation output data shall be normalized beforehand to
		// work, it cannont be normalized here,
		// it should be normalized as a whole before validating single rows
		double[][] targetData = validationData.getOutputData();
		ValidationResults results = new ValidationResults(
				validationData.getNumRows(),
				validationData.getNumOutputColumns());
		for (int i = 0; i < validationData.getNumRows(); i++) {
			double[] outputPattern = outputSynapse.getNextPattern();
			double[] targetPattern = targetData[i];
			results.addPattern(outputPattern, targetPattern);
		}

		net.stop();
		print("Validation finished: RMSE = " + results.getRootMeanSquaredError());
		return results;

	}

	/* NEURAL NET LISTENER METHODS */

	@Override
	public void netStarted(NeuralNetEvent e) {
	}

	@Override
	public void cicleTerminated(NeuralNetEvent e) {
	}

	@Override
	public void netStopped(NeuralNetEvent e) {
	}

	@Override
	public void errorChanged(NeuralNetEvent e) {
	}

	@Override
	public void netStoppedError(NeuralNetEvent e, String error) {
		print(" ERROR !  " + error);
		System.exit(1);

	}

	/* AUXILIAR METHODS */

	protected void print(String msg) {
		System.out.println(net.getDescriptor().getNeuralNetName() + ": " + msg);
	}

}
