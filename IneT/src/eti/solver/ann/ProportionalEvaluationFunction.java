package eti.solver.ann;

import java.util.Comparator;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public class ProportionalEvaluationFunction implements
		SolutionEvaluationFunction, Comparator<SingleSolution> {

	public SingleSolution getBestSolution(MultipleSolution ms) {
		SingleSolution[] ss = ms.getSolutions();
		SingleSolution bestSolution = ss[0];
		for (int i = 1; i < ss.length; i++) {
			if (compare(bestSolution, ss[i]) < 0) bestSolution = ss[i];
		}
		return ss[0];
	}

	@Override
	public int compare(SingleSolution arg0, SingleSolution arg1) {
		double diff = arg0.getOutputValue() - arg1.getOutputValue();
		if (diff < 0)
			return -1;
		else if (diff > 0)
			return 1;
		else
			return 0;
	}

}
