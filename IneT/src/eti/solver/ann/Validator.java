package eti.solver.ann;

import java.util.concurrent.Callable;

import org.joone.engine.NeuralNetListener;


/**
 * @author Mario Gomez-Martinez
 *
 */
public interface Validator extends Callable<ValidationResults>, NeuralNetListener {
	public ValidationResults validate();
}
