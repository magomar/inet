package eti.solver.ann;

import org.joone.engine.Layer;
import org.joone.engine.Monitor;
import org.joone.engine.NeuralNetEvent;
import org.joone.io.MemoryInputSynapse;
import org.joone.io.MemoryOutputSynapse;
import org.joone.net.NeuralNet;

/**
 * @author Mario Gomez-Martinez
 *
 */
public class SolveNetwork implements Solver {
	protected NeuralNet net;
	protected double[][] inputData;

	public SolveNetwork(NeuralNet nnet, double[][] inputData) {
		this.net = NeuralNetworkTools.cloneNet(nnet);
		this.inputData = inputData;
	}
	
	@Override
	public double[][] call() throws Exception {
		return solve();
	}
	
	
	@Override
	public double[][] solve() {	
		/* INPUT */
		Layer inputLayer = net.getInputLayer();
		inputLayer.removeAllInputs();
		MemoryInputSynapse inputSynapse = NeuralNetworkTools.createInput("Input Synapse",inputData);
		inputLayer.addInputSynapse(inputSynapse);

		/* DESIRED OUTPUT (TARGET) */
		Layer outputLayer = net.getOutputLayer();
		outputLayer.removeAllOutputs();
		MemoryOutputSynapse outputSynapse = new MemoryOutputSynapse();
		outputLayer.addOutputSynapse(outputSynapse);
		
		/* SOLVE  */
		// Get the Monitor object and set the learning parameters
		Monitor monitor = this.net.getMonitor();
		monitor.setTrainingPatterns(inputData.length);
		monitor.setLearning(false);
		monitor.setTotCicles(1);

		net.go(true);
		
		// CARE !!  output data shall be normalized beforehand to work, it cannot be normalized here, 
		// it should be normalized as a whole before validating single rows;
//		print("*** Solution (Output data) ***" );
		double[][] outputResults = new double[inputData.length][net.getOutputLayer().getRows()];
		for (int i = 0; i < outputResults.length; i++) {
			double[] outputPattern = outputSynapse.getNextPattern();
			outputResults[i] = outputPattern;
//			print("Pattern #" + i + ": "+ Arrays.toString(outputPattern));
		}
		
		net.stop();
		return outputResults;
		
	}
	

	/* NEURAL NET LISTENER METHODS */
	
	
	@Override
	public void netStarted(NeuralNetEvent e) {
	}

	@Override
	public void cicleTerminated(NeuralNetEvent e) {
	}

	@Override
	public void netStopped(NeuralNetEvent e) {
	}

	@Override
	public void errorChanged(NeuralNetEvent e) {
	}

	@Override
	public void netStoppedError(NeuralNetEvent e, String error) {
		print(" ERROR !  " + error);
		System.exit(1);
		
	}
	
	/* AUXILIAR METHODS */
	
	protected void print(String msg) {
		System.out.println(net.getDescriptor().getNeuralNetName() + ": " + msg);
	}

	
}
