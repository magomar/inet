package eti.solver.ann;

import org.joone.engine.FullSynapse;
import org.joone.engine.LinearLayer;
import org.joone.engine.Monitor;
import org.joone.engine.SigmoidLayer;
import org.joone.engine.learning.TeachingSynapse;
import org.joone.io.MemoryInputSynapse;
import org.joone.net.NeuralNet;

import eti.data.source.DataSource;


/**
 * @author Mario Gomez-Martinez
 *
 */
public class BuildNetwork implements Builder {
	protected NeuralNet net = null;
	protected LinearLayer inputLayer;
	protected SigmoidLayer hiddenLayer;
	protected SigmoidLayer outputLayer;
	protected TeachingSynapse teachingSynapse;

	public BuildNetwork(String neuralNetName, DataSource dataSource) {
		// create the network
		net = new NeuralNet();
		net.getDescriptor().setNeuralNetName(neuralNetName);
		
		// create the three Layers
		inputLayer = new LinearLayer("Input Layer");
		hiddenLayer = new SigmoidLayer("Hidden Layer");
		outputLayer = new SigmoidLayer("Output Layer");
		inputLayer.setRows(dataSource.getNumInputColumns());
		int numHiddenUnits = (dataSource.getNumInputColumns() + dataSource.getNumInputColumns())* 2 / 3;
		hiddenLayer.setRows(numHiddenUnits);
		outputLayer.setRows(dataSource.getNumOutputColumns());

		// create the two standard Synapses
		FullSynapse synapse_IH = new FullSynapse(); // input -> hidden conn.
		synapse_IH.setName("IHS");
		FullSynapse synapse_HO = new FullSynapse(); // hidden -> output conn.
		synapse_HO.setName("HOS");

		// Connect the input layer with the hidden layer, and the hidden layer whit the output layer
		NeuralNetworkTools.connectLayers(inputLayer, synapse_IH, hiddenLayer);
		NeuralNetworkTools.connectLayers(hiddenLayer, synapse_HO, outputLayer);
		
		// Create the teacher synapse and attach it to the output layer
		teachingSynapse = new TeachingSynapse();
		teachingSynapse.setName("Teacher");
		outputLayer.addOutputSynapse(teachingSynapse);
		
		
		/* INPUT  */
		MemoryInputSynapse inputSynapse = NeuralNetworkTools.createInput("Input Synapse", dataSource.getInputData());
		inputLayer.addInputSynapse(inputSynapse);

		/* DESIRED OUTPUT (TARGET) */
		MemoryInputSynapse targetSynapse = NeuralNetworkTools.createInput("Target Synapse", dataSource.getOutputData());
		teachingSynapse.setDesired(targetSynapse);

		/* TRAINING AND VALIDATION PATTERNS */
		Monitor monitor = net.getMonitor();
		monitor.setTrainingPatterns(dataSource.getNumRows());
		
		net.addLayer(inputLayer, NeuralNet.INPUT_LAYER);
		net.addLayer(hiddenLayer, NeuralNet.HIDDEN_LAYER);
		net.addLayer(outputLayer, NeuralNet.OUTPUT_LAYER);
		net.setTeacher(teachingSynapse);
	}

	@Override
	public NeuralNet getNnet() {
		return net;
	}

}
