/**
 * 
 */
package eti.solver.ann;

import org.joone.engine.Monitor;

/**
 * @author Mario Gomez-Martinez
 *
 */
public final class TrainingParameters {
	public static final double DEFAULT_LEARNING_RATE = 0.8;
	public static final double DEFAULT_MOMENTUM = 0.4;
	public static final int DEFAULT_EPOCHS = 10000;
	private double learningRate;
	private double momentum;
	private int epochs;
	
	public TrainingParameters() {
		this(DEFAULT_LEARNING_RATE, DEFAULT_MOMENTUM, DEFAULT_EPOCHS);
	}

	public TrainingParameters(double learningRate, double momentum, int epochs) {
		this.learningRate = learningRate;
		this.momentum = momentum;
		this.epochs = epochs;
	}
	
	public void setTrainingParameters(Monitor monitor) {
		monitor.setLearningRate(learningRate);
		monitor.setMomentum(momentum);
		monitor.setTotCicles(epochs);
		monitor.setLearning(true); // The net must be trained
	}

	public double getLearning_rate() {
		return learningRate;
	}

	public void setLearning_rate(double learning_rate) {
		this.learningRate = learning_rate;
	}

	public double getMomentum() {
		return momentum;
	}

	public void setMomentum(double momentum) {
		this.momentum = momentum;
	}

	public int getEpochs() {
		return epochs;
	}

	public void setEpochs(int epochs) {
		this.epochs = epochs;
	}
	
	@Override
	public String toString() {
		return "(" + learningRate + ", " + momentum + ", " + epochs + ")";
	}

}
