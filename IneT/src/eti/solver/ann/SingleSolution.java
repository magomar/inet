package eti.solver.ann;

import eti.data.tools.DataTools;

/**
 * @author Mario Gomez-Martinez
 * 
 */
public class SingleSolution {
	private final double[] outputData;
	private final double outputValue;
	private int conditionIndex;

	/**
	 * @param outputData
	 */
	public SingleSolution(double[] outputData, int conditionIndex) {
		this.outputData = outputData;
		double value = 0.0;
		for (int j = 0; j < outputData.length; j++) {
			value += outputData[j];
		}
		outputValue = value / outputData.length;
	}

	/**
	 * @return the outputValue
	 */
	public double getOutputValue() {
		return outputValue;
	}

	/**
	 * @return the conditionIndex
	 */
	public int getConditionIndex() {
		return conditionIndex;
	}

	/**
	 * @return the outputData
	 */
	public double[] getOutputData() {
		return outputData;
	}

	@Override
	public String toString() {
		return DataTools.toString(outputData);
	}
}
